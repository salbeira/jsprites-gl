#version 400

layout(std140) uniform Environment {
	vec2 canvas_size;
};

layout(std140) uniform Sprite {
	vec2 position;
	vec2 size;
	vec2 position_in_texture;
	vec2 size_in_texture;
	float rotation;
};

layout(location=0) in vec2 vertex;

out vec2 texture_coordinate;

void main(){
	//Calculate texture coordinate
	texture_coordinate = vec2(position_in_texture.x + vertex.x * size_in_texture.x,
	                          1 - (position_in_texture.y + vertex.y * size_in_texture.y));
	//Calculate Screen Position
	vec2 temp = vec2(vertex.x * size.x + position.x , vertex.y * size.y + position.y);
	
	vec2 pos = vec2(temp.x * 2 / canvas_size.x - 1, temp.y * (-2) / canvas_size.y + 1);
	
	vec2 real_size = vec2(size.x / canvas_size.x , size.y / canvas_size.y);
	vec2 center = vec2(position.x + size.x / 2, position.y + size.y / 2);
	center = vec2(center.x * 2 / canvas_size.x - 1, center.y * (-2) / canvas_size.y + 1);
	vec2 rot = pos - center;
	rot = rot * mat2(cos(rotation), sin(rotation), -sin(rotation), cos(rotation));
	pos = rot + center;
	
	gl_Position = vec4(pos, -1.0, 1.0);
}