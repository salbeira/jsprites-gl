#version 400

uniform sampler2D color_map;

layout(std140) uniform Sprite {
	vec2 position;
	vec2 size;
	vec2 position_in_texture;
	vec2 size_in_texture;
	float rotation;
};

in vec2 texture_coordinate;

layout(std140) uniform Color {
	vec4 color;
};

layout(location = 0) out vec4 out_color;

void main(){
	vec4 tex_color = texture2D(color_map, texture_coordinate);
	vec2 diff;
	vec2 norm;
	vec2 sample_coordinate;
	out_color = color;
	int radius = 32;
	
	norm = vec2(radius/min(size.x, size.y), radius/min(size.x, size.y));
	
//	if(tex_color != vec4(0.0, 0.0, 0.0, 0.0)){
		//out_color = mix(out_color, color, color.a);
//		out_color = color;
//	} else {
		for(int i=-radius; i < radius; i++){
			for(int j=-radius; j < radius; j++){
				diff = vec2(i/size.x, j/size.y);
				sample_coordinate = texture_coordinate + diff;
				if(0.0 <= sample_coordinate.x && sample_coordinate.x <= 1.0 && 0.0 <= sample_coordinate.y && sample_coordinate.y <= 1.0){
					tex_color = texture2D(color_map, sample_coordinate);
					if(tex_color.a != 0 /*&& length(diff) <= length(norm)*/){
						float influence = 1 - length(diff) / length(norm);
						out_color = mix(out_color, tex_color, pow(influence,2));
					}
				}
			}
		}
//	}
}