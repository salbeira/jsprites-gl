#version 400

uniform sampler2D scene;
uniform sampler2D light;

layout(std140) uniform Sprite {
	vec2 position;
	vec2 size;
	vec2 position_in_texture;
	vec2 size_in_texture;
	float rotation;
};

layout(std140) uniform Color {
	vec4 color;
};

in vec2 texture_coordinate;

layout(location = 0) out vec4 out_color;

float dither(int x, int y, float a) {

	/* Array taken from: http://devlog-martinsh.blogspot.fi/2011/03/glsl-8x8-bayer-matrix-dithering.html */
	
	int pattern[64] = int[]( 0, 32,  8, 40,  2, 34, 10, 42,
							48, 16, 56, 24, 50, 18, 58, 26,
							12, 44,  4, 36, 14, 46,  6, 38,
							60, 28, 52, 20, 62, 30, 54, 22,
							 3, 35, 11, 43,  1, 33,  9, 41,
							51, 19, 59, 27, 49, 17, 57, 25,
							15, 47,  7, 39, 13, 45,  5, 37,
							63, 31, 55, 23, 61, 29, 53, 21); 
	if(float((pattern[ int(mod(x,8) * 8) + int(mod(y,8)) ]) + 1.0) / 64.0 < a){
		return 1.0;
	} else {
		return 0.0;
	}
}

void main(){
	vec4 scene_color = texture(scene, texture_coordinate);
	vec4 light_color = texture(light, texture_coordinate);
	float render = dither(int(texture_coordinate.x * size.x), int(texture_coordinate.y * size.y), light_color.a);
	if(render != 0.0){
		//out_color = vec4(scene_color.x, scene_color.y, scene_color.z, 1.0);
		out_color = vec4(scene_color.x * light_color.x, scene_color.y * light_color.y, scene_color.z * light_color.z, 1.0);
	} else {
		out_color = vec4(0,0,0,1);
	}
}