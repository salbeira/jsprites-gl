#version 400

uniform sampler2D color_map;

in vec2 texture_coordinate;

layout(std140) uniform Color {
	vec4 color;
};

layout(location = 0) out vec4 out_color;

void main(){
	
//  Get the color of the current pixel being rendered from texture
	vec4 tex_color = texture2D(color_map, texture_coordinate);
//  Mix the colors:
    out_color = vec4(tex_color.r * (1.0 - color.a) + color.r * color.a ,
			tex_color.g * (1.0 - color.a) + color.g * color.a ,
			tex_color.b * (1.0 - color.a) + color.b * color.a ,
			tex_color.a);
//	out_color = color;
	
}