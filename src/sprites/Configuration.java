package sprites;

import java.util.LinkedList;
import java.util.List;

import sprites.base.ConfigurationObserver;
import sprites.io.Keyboard;
import sprites.io.Mouse;

public class Configuration {

	/* Window and Canvas size */
	private int windowWidth;
	private int windowHeight;
	
	private int canvasWidth;
	private int canvasHeight;
	
	/* Callback Functions */
	
	private RenderingCallback renderingCallback;
	private LogicCallback logicCallback;
	
	/* Keyboard and Mouse */
	
	private Keyboard keyboard;
	private Mouse mouse;
	
	/* Window interaction behavior */
	
	private boolean windowResizable;
	private boolean fullscreen;
	private boolean undecorated;
	
	private List<ConfigurationObserver> observerList;
	
	public Configuration(){
		this(null, null);
	}
	
	public Configuration(RenderingCallback renderingCallback, LogicCallback logicCallback){
		this(renderingCallback, logicCallback, 800, 600, 800, 600);
	}
	
	public Configuration(RenderingCallback renderingCallback, LogicCallback logicCallback, int windowWidth, int windowHeight, int canvasWidth, int canvasHeight){
		this.renderingCallback = renderingCallback;
		this.logicCallback = logicCallback;
		this.windowWidth = windowWidth;
		this.windowHeight = windowHeight;
		this.canvasWidth = canvasWidth;
		this.canvasHeight = canvasHeight;
		
		this.windowResizable = false;
		
		this.keyboard = new Keyboard();
		this.mouse = new Mouse();
		
		this.observerList = new LinkedList<ConfigurationObserver>();
	}
	
	public void subscribe(ConfigurationObserver observer){
		this.observerList.add(observer);
	}
	
	public void unsubscribe(ConfigurationObserver observer){
		this.observerList.remove(observer);
	}

	public int getWindowWidth() {
		return windowWidth;
	}

	public int getWindowHeight() {
		return windowHeight;
	}
	
	/**
	 * Sets the internal values of the window's dimensions to the specified values and notifies all observers of the change.
	 * @param windowWidth
	 * @param windowHeight
	 */
	public void setWindowSize(int windowWidth, int windowHeight) {
		if(this.windowWidth != windowWidth || this.windowHeight != windowHeight){
			this.windowWidth = windowWidth;
			this.windowHeight = windowHeight;
			for(ConfigurationObserver obs : observerList){
				obs.changedWindowSize(windowWidth, windowHeight);
			}
		}
	}

	public int getCanvasWidth() {
		return this.canvasWidth;
	}

	public int getCanvasHeight() {
		return this.canvasHeight;
	}
	
	public void setCanvasSize(int canvasWidth, int canvasHeight) {
		if(this.canvasWidth != canvasWidth || this.canvasHeight != canvasHeight){
			this.canvasWidth = canvasWidth;
			this.canvasHeight = canvasHeight;
			for(ConfigurationObserver obs : observerList){
				obs.changedCanvasSize(canvasWidth, canvasHeight);
			}
		}
	}

	public RenderingCallback getRenderingCallback() {
		return renderingCallback;
	}

	public void setRenderingCallback(RenderingCallback renderingCallback) {
		this.renderingCallback = renderingCallback;
		for(ConfigurationObserver obs : observerList){
			obs.changedRenderingCallback(renderingCallback);
		}
	}

	public LogicCallback getLogicCallback() {
		return this.logicCallback;
	}

	public void setLogicCallback(LogicCallback logicCallback) {
		this.logicCallback = logicCallback;
		for(ConfigurationObserver obs : this.observerList){
			obs.changedLogicCallback(logicCallback);
		}
	}

	public Keyboard getKeyboard() {
		return this.keyboard;
	}

	public void setKeyboard(Keyboard keyboard) {
		this.keyboard = keyboard;
		for(ConfigurationObserver obs : this.observerList){
			obs.changedKeyboard(keyboard);
		}
	}

	public Mouse getMouse() {
		return this.mouse;
	}

	public void setMouse(Mouse mouse) {
		this.mouse = mouse;
		for(ConfigurationObserver obs : observerList){
			obs.changedMouse(mouse);
		}
	}
	
	public boolean isWindowResizable(){
		return this.windowResizable;
	}
	
	/**
	 * Sets weather or not the application's window should be resizable.<br\>
	 * On some systems, changing the resizable flag may cause it's theme to change,<br\>
	 * and thus it's appearance and size. As of yet, this will possibly happen on Windows systems.<br\>
	 * I recommend not allowing the user to change this value once the application was started.
	 * @param windowResizable
	 */
	public void setWindowResizable(boolean windowResizable){
		if(this.windowResizable != windowResizable){
			for(ConfigurationObserver obs : observerList){
				obs.changedWindowResizable(windowResizable);
			}
		}
	}

	public boolean isFullscreen() {
		return fullscreen;
	}

	public void setFullscreen(boolean fullscreen) {
		if(this.fullscreen != fullscreen){
			this.fullscreen = fullscreen;
			for(ConfigurationObserver obs : observerList){
				obs.changedWindowFullscreen(fullscreen);
			}
		}
	}

	public boolean isUndecorated() {
		return undecorated;
	}

	/**
	 * Allows changing the decoration of the active window.<br\>
	 * If the decoration is changed, the current OpenGL Context might<br\>
	 * be destroyed and recreated, causing init() of the<br\>
	 * RenderingCallback to be called again. Be ready for this if<br\>
	 * the user is allowed to change this flag after the application was started.
	 * @param undecorated
	 */
	public void setUndecorated(boolean undecorated) {
		if(this.undecorated != undecorated){
			this.undecorated = undecorated;
			for(ConfigurationObserver obs : observerList){
				obs.changedWindowUndecorated(undecorated);
			}
		}
	}
}