package sprites;

import sprites.base.ogl.Framebuffer;
import sprites.base.ogl.ShaderProgram;
import sprites.io.Keyboard;
import sprites.io.Mouse;
import sprites.util.BitmapFont;

public interface Engine {
	
	public void setColor(float red, float green, float blue, float intensity);
	
	/**
	 * Renders the given String at the given position with the x and y coordinates<br>
	 * given being the top left corner of the leftmost letter of the given String.
	 * @param string The text that should be rendered.
	 * @param x The x coordinate of the top left corner.
	 * @param y The y coordinate of the top left corner.
	 * @param font The bitmap font being used to render the text.
	 */
	public void renderString(String string, int x , int y, BitmapFont font);
	
	/**
	 * Renders the given String at the given position with it's center at the x and y coordinates<br>
	 * given. The whole text written is centered around that position, also aligining the text to the center.
	 * @param string The text that should be rendered.
	 * @param x The x coordinate of the center.
	 * @param y The y coordinate of the center.
	 * @param font
	 */
	public void renderStringCentered(String string, int x, int y, BitmapFont font);
	
	/**
	 * Renders the given Sprite at the given position with the x and y coordinates<br>
	 * given representing the top left corner where the Sprite will be rendered.
	 * @param sprite The Sprite that should be rendered.
	 * @param x The x coordinate of the top left corner.
	 * @param y The y coordinate of the top right corner.
	 */
	public void renderSprite(Sprite sprite, int x, int y);
	
	/**
	 * Renders the given Sprite at the given position as the center of that sprite.
	 * @param sprite The Sprite that should be rendered.
	 * @param x The x coordinate of the center.
	 * @param y The y coordinate of the center.
	 */
	public void renderSpriteCentered(Sprite sprite, int x, int y);
	
	/**
	 * 
	 * @param sprite
	 * @param x
	 * @param y
	 * @param phi
	 */
	public void renderSpriteRotated(Sprite sprite, int x, int y, float phi);
	
	/**
	 * 
	 * @param x
	 * @param y
	 * @param x_radius
	 * @param y_radius
	 */
	public void addLight(int x, int y, int x_radius, int y_radius);
	
	/**
	 * 
	 * @param width
	 * @param height
	 * @return
	 */
	public Framebuffer createFramebuffer(int width, int height);
	
	/**
	 * 
	 * @param framebuffer
	 */
	public void useFramebuffer(Framebuffer framebuffer);
	
	/**
	 * 
	 * @param framebuffer
	 */
	public void clearFramebuffer(Framebuffer framebuffer, float red, float green, float blue, float alpha);
	
	/**
	 * 
	 * @return
	 */
	public ShaderProgram getDefaultProgram();
	
	/**
	 * 
	 * @param url
	 * @return
	 */
	public Texture createTexture(String url);
	
	/**
	 * 
	 * @param url
	 * @return
	 */
	public Texture createMipmappedTexture(String url);
	
	/**
	 * 
	 * @return
	 */
	public Keyboard getKeyboard();
	
	/**
	 * 
	 * @return
	 */
	public Mouse getMouse();
	
	/**
	 * 
	 * @return
	 */
	public Configuration getConfiguration();
}