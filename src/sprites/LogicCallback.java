package sprites;

/**
 * A LogicCallback is used as a way to integrate game logic into the application's loop.
 * The logic will be called whenever possible, but with regards to trying to keep up
 * a certain framerate.
 * 
 * @author hauer
 */
public interface LogicCallback {
	
	/**
	 * The tick method is being called whenever there is time to calculate a new state of
	 * the game model.
	 * @param delta The time elapsed since the last call.
	 */
	public void tick(long delta);
	
}
