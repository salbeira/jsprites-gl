package sprites;

public interface RenderingCallback {
	
	public void render(Engine engine);
	public void init(Engine engine);
	
}
