package sprites;

import sprites.Texture;

public class Sprite {

	private Texture texture;

	private int width;
	private double textureX; 
	private double widthInTexture;
	private int height;
	private double textureY;
	private double heightInTexture;
	
	public Sprite(Texture texture){
		this(texture, texture.getWidth(), texture.getHeight(), 0, 0, texture.getWidthRatio(), texture.getHeightRatio());
	}
	
	public Sprite(Texture texture, int x, int y, int width, int height){
		this(texture, width, height,
				(double) x / (double) texture.getWidth() * texture.getWidthRatio(),
				(double) y / (double) texture.getHeight() * texture.getHeightRatio(),
				(double) width / texture.getWidth() * texture.getWidthRatio(),
				(double) height / texture.getHeight() * texture.getHeightRatio());
	}
	
	public Sprite(Texture texture, int width, int height, double textureX, double textureY, double widthInTexture, double heightInTexture) {
		this.texture = texture;
		this.width = width;
		this.height = height;
		this.textureX = textureX;
		this.textureY = textureY;
		this.widthInTexture = widthInTexture;
		this.heightInTexture = heightInTexture;
	}

	public int getWidth() {
		return this.width;
	}
	
	public int getHeight() {
		return this.height;
	}
	
	public double getTextureX(){
		return this.textureX;
	}
	
	public double getTextureY(){
		return this.textureY;
	}
	
	public double getWidthInTexture(){
		return this.widthInTexture;
	}
	
	public double getHeightInTexture(){
		return this.heightInTexture;
	}
	
	public Texture getTexture(){
		return this.texture;
	}
}
