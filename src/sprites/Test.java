package sprites;

import sprites.base.SpritesListener;
import sprites.base.ogl.Framebuffer;
import sprites.util.BavariaRegularBitmapFont;
import sprites.util.BitmapFont;


public class Test implements RenderingCallback{

	private Sprite spritesLogo;

	private BitmapFont font;

	
	private Sprite grass;
	
	private int size = 1024;
	private int plus = -1;
	
	public static void main(String... args){
		
		SpritesApplication app = new SpritesApplication();
		
		Configuration config = app.getConfiguration();
		
		config.setRenderingCallback(new Test());
		config.setWindowResizable(false);
		
		app.start();
	}
	
	@Override
	public void init(Engine engine) {
		Texture texture = engine.createTexture("res/assets/sprites.png");
		this.spritesLogo = new Sprite(texture);
		Texture font = engine.createTexture("res/assets/fonts/bavaria_regular_font.png");
		this.font = new BavariaRegularBitmapFont(font);
		this.grass = new Sprite(engine.createTexture("res/assets/grass.png"));
	}
	
	@Override
	public void render(Engine engine) {
		long time = System.nanoTime();
		for(int i = 0; i < engine.getConfiguration().getCanvasWidth(); i += 32){
			for(int j = 0; j < engine.getConfiguration().getCanvasHeight(); j += 32){
				engine.renderSprite(this.grass, i, j);
			}
		}
		engine.renderStringCentered("Grass time: "+(System.nanoTime()-time), 400, 16, font);
		testInverseLighting(engine);
		engine.renderStringCentered("Frame time: "+(System.nanoTime()-time), 400, 8, font);
		engine.renderString("Current light size: "+size, 0, 0, font);
	}
	
	private void testInverseLighting(Engine engine){
		size += plus;
		plus = 2 > size || size > 1023 ? -plus : plus;
		engine.addLight(400, 300, size, size);
		engine.setColor(0.7f, 0.3f, 0.3f, 1f);
		engine.addLight(800-32, 600-32, 32, 32);
		engine.setColor(1.0f, 0.0f, 0.0f, 1f);
		engine.addLight(32, 32, 64, 64);
		engine.setColor(0.0f, 1.0f, 0.0f, 1f);
		engine.addLight(48, 32, 64, 64);
		engine.setColor(0.0f, 0.0f, 1.0f, 1f);
		engine.addLight(40, 40, 64, 64);
		engine.setColor(0.0f, 0.0f, 0.0f, 0.0f);
	}
}