package sprites;

public class Texture {

	private int textureID;

	private int	storedHeight;

	private int	storedWidth;

	private int	textureWidth;

	private int	textureHeight;
	
	/**
	 * Create a new texture
	 *
	 * @param target The GL target
	 * @param textureID The GL texture ID
	 */
	public Texture(int textureID, int storedWidth, int storedHeight, int textureWidth, int textureHeight) {
		this.textureID = textureID;
		this.storedWidth = storedWidth;
		this.storedHeight = storedHeight;
		this.textureWidth = textureWidth;
		this.textureHeight = textureHeight;
	}
	
	/**
	 * 
	 * @return The id this texture is associated with
	 */
	public int getID(){
		return this.textureID;
	}
	
	/**
	 * 
	 * @return The height the texture image has in pixel
	 */
	public int getHeight() {
		return this.textureHeight;
	}
	
	/**
	 * 
	 * @return The width the texture image has in pixel
	 */
	public int getWidth() {
		return this.textureWidth;
	}
	
	/**
	 * 
	 * @return The ratio at which the texture image's width differs from the width the texture is stored
	 */
	public float getWidthRatio() {
		return ((float) textureWidth) / storedWidth;
	}
	

	/**
	 * 
	 * @return The ratio at which the texture image's height differs from the height the texture is stored
	 */
	public float getHeightRatio() {
		return ((float) textureHeight) / storedHeight;
	}
}
