package sprites.base;

import sprites.LogicCallback;
import sprites.RenderingCallback;
import sprites.io.Keyboard;
import sprites.io.Mouse;

public interface ConfigurationObserver {

	public void changedWindowSize(int width, int height);
	public void changedCanvasSize(int width, int height);
	public void changedWindowFullscreen(boolean fullscreen);
	public void changedWindowResizable(boolean resizable);
	public void changedWindowUndecorated(boolean undecorated);
	public void changedScaleCanvasOnResize(boolean scaleCanvasOnResize);
	public void changedMouse(Mouse mouse);
	public void changedKeyboard(Keyboard keyboard);
	public void changedLogicCallback(LogicCallback logicCallback);
	public void changedRenderingCallback(RenderingCallback renderingCallback);	
}
