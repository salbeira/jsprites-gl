package sprites.base;

import static javax.media.opengl.GL.GL_BLEND;
import static javax.media.opengl.GL.GL_COLOR_ATTACHMENT0;
import static javax.media.opengl.GL.GL_COLOR_BUFFER_BIT;
import static javax.media.opengl.GL.GL_CULL_FACE;
import static javax.media.opengl.GL.GL_DEPTH_ATTACHMENT;
import static javax.media.opengl.GL.GL_DEPTH_BUFFER_BIT;
import static javax.media.opengl.GL.GL_DEPTH_COMPONENT32;
import static javax.media.opengl.GL.GL_DEPTH_TEST;
import static javax.media.opengl.GL.GL_FRAMEBUFFER;
import static javax.media.opengl.GL.GL_FRAMEBUFFER_COMPLETE;
import static javax.media.opengl.GL.GL_LEQUAL;
import static javax.media.opengl.GL.GL_NEAREST;
import static javax.media.opengl.GL.GL_ONE_MINUS_SRC_ALPHA;
import static javax.media.opengl.GL.GL_RENDERBUFFER;
import static javax.media.opengl.GL.GL_RGBA;
import static javax.media.opengl.GL.GL_SRC_ALPHA;
import static javax.media.opengl.GL.GL_TEXTURE_2D;
import static javax.media.opengl.GL.GL_TEXTURE_MAG_FILTER;
import static javax.media.opengl.GL.GL_TEXTURE_MIN_FILTER;
import static javax.media.opengl.GL.GL_TRIANGLES;
import static javax.media.opengl.GL.GL_UNSIGNED_BYTE;
import static javax.media.opengl.GL4.GL_UNIFORM_BUFFER;
import static javax.media.opengl.GL4.GL_UNIFORM_OFFSET;

import static javax.media.opengl.GL4.*;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import javax.media.opengl.GL4;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLEventListener;

import sprites.Configuration;
import sprites.Engine;
import sprites.Sprite;
import sprites.Texture;
import sprites.base.ogl.Framebuffer;
import sprites.base.ogl.Shader;
import sprites.base.ogl.ShaderProgram;
import sprites.io.Keyboard;
import sprites.io.Mouse;
import sprites.util.BitmapFont;
import sprites.util.Log;
import sprites.util.SpritesUtility;

public class SpritesListener implements GLEventListener, Engine{

	private GL4 gl;
	
	private int ENV_UBO_SIZE = 2;
	private int SPRITE_UBO_SIZE = 9;
	private int COLOR_UBO_SIZE = 4;
	
	private ShaderProgram defaultProgram;
	private ShaderProgram ditherLightingProgram;
	
	private Texture light;
	
	private Framebuffer sceneFB;
	private Sprite sceneSprite;
	private Framebuffer lightFB;
	
	private int spriteVAO;
	private int envBuffer;
	private int sprBuffer;
	private int colorBuffer;
	
	private Configuration config;
	
	private IntBuffer ID_BUFFER;
	private int boundFramebuffer;
	
	private SpritesTextureDatabase db;
	
	public SpritesListener(Configuration configuration){
		this.config = configuration;
		if(this.config == null){
			throw new IllegalArgumentException("Configuration is null");
		}
		this.ID_BUFFER = IntBuffer.allocate(1);
		this.boundFramebuffer = 0;
		this.db = new SpritesTextureDatabase();
	}
	
	@Override
	public Keyboard getKeyboard() {
		return this.config.getKeyboard();
	}
	
	@Override
	public Mouse getMouse() {
		return this.config.getMouse();
	}
	
	@Override
	public Configuration getConfiguration() {
		return this.config;
	}
	
	@Override
	public ShaderProgram getDefaultProgram() {
		return this.defaultProgram;
	}
	
	@Override
	public void setColor(float red, float green, float blue, float intensity) {
		this.setColorUBO(red, green, blue, intensity);
	}
	
	@Override
	public void renderSprite(Sprite sprite, int x, int y) {
		this.renderSpriteRotated(sprite, x, y, 0);
	}
	
	@Override
	public void renderSpriteRotated(Sprite sprite, int x, int y, float phi){
		Texture t = sprite.getTexture();
		
		int xpos = x;
		int ypos = y;
		int width = sprite.getWidth();
		int height = sprite.getHeight();
		float xit = (float) sprite.getTextureX();
		float yit = (float) sprite.getTextureY();
		float wit = (float) sprite.getWidthInTexture();
		float hit = (float) sprite.getHeightInTexture();
		
		this.setSpriteUBO(xpos, ypos, width, height, xit, yit, wit, hit, phi);
		
		this.gl.glBindVertexArray(this.spriteVAO);
		this.gl.glBindTexture(GL_TEXTURE_2D, t.getID());
		
		this.gl.glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_BYTE, 0);
		
		this.gl.glBindTexture(GL_TEXTURE_2D, 0);
		this.gl.glBindVertexArray(0);
	}
	
	@Override
	public void renderSpriteCentered(Sprite sprite, int x, int y) {
		this.renderSprite(sprite, x - sprite.getWidth() / 2, y - sprite.getHeight() / 2);
	}
	
	@Override
	public void renderString(String string, int x , int y, BitmapFont font){
		String[] lines = string.split("\n");
		int height = 0;
		for(int i = 0; i < lines.length ; i++){
			int width = 0;
			for(int j = 0; j < lines[i].length(); j++){
				Sprite glyph = font.getGlyph(lines[i].charAt(j));
				if(glyph != null){
					this.renderSprite(glyph, x + width, y + height);
				}
				width += glyph.getWidth();
			}
			height += font.getGlyphHeight();
		}
	}
	
	@Override
	public void renderStringCentered(String string, int x, int y,
			BitmapFont font) {
		int boxWidth = font.getStringWidth(string);
		int boxHeight = font.getStringHeight(string);
		
		int boxX = x - (boxWidth / 2);
		int boxY = y - (boxHeight / 2);
		
		String[] lines = string.split("\n");
		int height = 0;
		for(int i = 0; i < lines.length ; i++){
			int size = font.getStringWidth(lines[i]);
			this.renderString(lines[i], boxX + ((boxWidth - size) / 2), boxY + height, font);
			height += font.getGlyphHeight();
		}
	}
	
	@Override
	public void addLight(int x, int y, int x_radius, int y_radius){
		this.gl.glBindFramebuffer(GL_FRAMEBUFFER, this.lightFB.getID());
		this.gl.glUseProgram(this.defaultProgram.getID());
		//TODO IFF custom shaders are allowed, add restoring state here
		this.renderSpriteCentered(new Sprite(this.light,x_radius*2, y_radius*2, 0, 0, 1, 1), x, y);
		this.gl.glBindFramebuffer(GL_FRAMEBUFFER, this.boundFramebuffer);
	}
	
	@Override
	public Framebuffer createFramebuffer(int width, int height){
		int fb = 0;
		this.gl.glGenFramebuffers(1, ID_BUFFER);
		fb = ID_BUFFER.get(0);
		
		if(fb == 0) throw new IllegalStateException("Unable to allocate a new framebuffer");
		
		this.gl.glBindFramebuffer(GL_FRAMEBUFFER, fb);
		
		// The texture we're going to render to
		int renderedTexture;
		this.gl.glGenTextures(1, ID_BUFFER);
		renderedTexture = ID_BUFFER.get(0);
		 
		// "Bind" the newly created texture : all future texture functions will modify this texture
		this.gl.glBindTexture(GL_TEXTURE_2D, renderedTexture);
		 
		// Give an empty image to OpenGL ( the last "0" )
		this.gl.glTexImage2D(GL_TEXTURE_2D, 0,GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, null);
		
		// Poor filtering. Needed !
		this.gl.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		this.gl.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		
		Texture texture = new Texture(renderedTexture, width, height, width, height);
		
		// The depth buffer
		int depthrenderbuffer;
		this.gl.glGenRenderbuffers(1, ID_BUFFER);
		depthrenderbuffer = ID_BUFFER.get(0);
		this.gl.glBindRenderbuffer(GL_RENDERBUFFER, depthrenderbuffer);
		this.gl.glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT32, width, height);
		this.gl.glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthrenderbuffer);
		
		// Set "renderedTexture" as our colour attachement #0
		this.gl.glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, renderedTexture, 0);
		 
		// Set the list of draw buffers.
		IntBuffer drbuf = IntBuffer.allocate(1);
		drbuf.put(GL_COLOR_ATTACHMENT0);
		drbuf.flip();
		this.gl.glDrawBuffers(1, drbuf); // "1" is the size of DrawBuffers
		
		if(this.gl.glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
			throw new IllegalStateException("ERROR: The internal state of the setup framebuffer seems to be corrupted");
		
		this.gl.glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		
		this.gl.glBindTexture(GL_TEXTURE_2D, 0);
		this.gl.glBindFramebuffer(GL_FRAMEBUFFER, 0);
		
		int error;
		if((error = this.gl.glGetError()) != 0){
			System.out.println("ERROR: Error while trying to connect environment buffer to program: "+ error);
		} else {
			//System.out.println("DEBUG: VBA and VBOs created");
		}
		
		return new Framebuffer(fb, texture);
	}
	
	/**
	 * 
	 */
	@Override
	public void useFramebuffer(Framebuffer framebuffer){
		if(framebuffer == null){
			this.gl.glBindFramebuffer(GL_FRAMEBUFFER, this.sceneFB.getID());
			this.boundFramebuffer = this.sceneFB.getID();
			this.gl.glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			this.gl.glViewport(0, 0, this.config.getWindowWidth(), this.config.getWindowHeight());
			this.setEnvironmentUBO(this.config.getCanvasWidth(), this.config.getCanvasHeight());
		} else {
			this.gl.glBindFramebuffer(GL_FRAMEBUFFER, framebuffer.getID());
			this.boundFramebuffer = framebuffer.getID();
			this.gl.glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			this.gl.glViewport(0, 0, framebuffer.getTexture().getWidth(), framebuffer.getTexture().getHeight());
			this.setEnvironmentUBO(framebuffer.getTexture().getWidth(), framebuffer.getTexture().getHeight());
		}
	}
	
	@Override
	public void clearFramebuffer(Framebuffer framebuffer, float red, float green, float blue, float alpha) {
		if(framebuffer == null){
			return;
		}
		this.gl.glBindFramebuffer(GL_FRAMEBUFFER, framebuffer.getID());
		this.gl.glClearColor(red, green, blue, alpha);
		this.gl.glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		this.gl.glBindFramebuffer(GL_FRAMEBUFFER, this.boundFramebuffer);
		
		int error;
		if((error = this.gl.glGetError()) != 0){
			System.out.println("ERROR: Error while trying to connect environment buffer to program: "+ error);
		} else {
			//System.out.println("DEBUG: VBA and VBOs created");
		}
	}
	
	private void useProgram(ShaderProgram program) {
		this.gl.glUseProgram(program.getID());
	}
	
	/**
	 * Get's called after the current OpenGL Context was made current and ready by a call of display() on an OpenGL canvas,
	 * that this listener is registered at. <br/>
	 * Clears the screen, resets the internal state to a default and then calls the render(Engine) callback of the user's application,
	 * defined within the Engine's Configuration.
	 * @param drawable
	 */
	@Override
	public void display(GLAutoDrawable drawable) {
		this.gl = drawable.getGL().getGL4();
//		this.gl = new DebugGL4(this.gl);
		
		this.useProgram(defaultProgram);
		this.useFramebuffer(this.sceneFB);
		this.setColor(0, 0, 0, 0);
		this.gl.glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		this.clearFramebuffer(sceneFB, 0, 0, 0, 1);
		this.clearFramebuffer(lightFB, 1, 1, 1, 0);
		
		if(this.config.getRenderingCallback() != null){
			this.config.getRenderingCallback().render(this);
		}
		
		this.useProgram(this.ditherLightingProgram);
		this.gl.glBindFramebuffer(GL_FRAMEBUFFER, 0);
		this.renderSprite(this.sceneSprite, 0, 0);
		
		//DEBUG
//		this.useProgram(this.defaultProgram);
//		this.renderSprite(new Sprite(this.lightFB.getTexture()), 0, 0);
	}
	
	private void setEnvironmentUBO(int width, int height){
		FloatBuffer env = FloatBuffer.allocate(ENV_UBO_SIZE);
		env.put(new float[]{width, height});
		env.flip();

		this.gl.glBindBuffer(GL_UNIFORM_BUFFER, envBuffer);
		this.gl.glBufferSubData(GL_UNIFORM_BUFFER, 0, ENV_UBO_SIZE * 4 , env);
		this.gl.glBindBuffer(GL_UNIFORM_BUFFER, 0);
	}
	
	private void setSpriteUBO(int xpos, int ypos, int width, int height, float xit, float yit, float wit, float hit, float rotation){
		float[] spriteArray = new float[]{xpos, ypos, width, height, xit, yit, wit, hit, rotation};
		
		FloatBuffer spriteBuffer = FloatBuffer.allocate(SPRITE_UBO_SIZE);
		spriteBuffer.put(spriteArray);
		spriteBuffer.flip();
		
		this.gl.glBindBuffer(GL_UNIFORM_BUFFER, this.sprBuffer);
		this.gl.glBufferSubData(GL_UNIFORM_BUFFER, 0, SPRITE_UBO_SIZE * 4, spriteBuffer);
		this.gl.glBindBuffer(GL_UNIFORM_BUFFER, 0);
	}
	
	private void setColorUBO(float red, float green, float blue, float intensity){
		FloatBuffer color = FloatBuffer.allocate(COLOR_UBO_SIZE);
		color.put(new float[]{red, green, blue, intensity});
		color.flip();

		this.gl.glBindBuffer(GL4.GL_UNIFORM_BUFFER, colorBuffer);
		this.gl.glBufferSubData(GL4.GL_UNIFORM_BUFFER, 0, COLOR_UBO_SIZE * 4, color);
		this.gl.glBindBuffer(GL4.GL_UNIFORM_BUFFER, 0);
	}
	
	@Override
	public void dispose(GLAutoDrawable drawable) {
		System.out.println("INFO: Listener Disposed");
		
	}
	
	@Override
	public void init(GLAutoDrawable drawable) {
		int error = 0;
		this.gl = drawable.getGL().getGL4();
//		this.gl = new DebugGL4(gl);
		System.out.println(this.gl.glGetString(GL4.GL_VERSION));
		IntBuffer buffer = IntBuffer.allocate(2);
		this.gl.glGetIntegerv(GL_MAX_TEXTURE_IMAGE_UNITS, buffer);
		System.out.println("Available Texture Units on this machine: " + buffer.get(0) + " Unknown Value: " + buffer.get(1));
		/* Enable Texturing with Alpha-Values */
		this.gl.glEnable(GL_BLEND);
		this.gl.glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		
		/* Set Clear values */
		this.gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		this.gl.glClearDepth(0);
		
		this.gl.glDisable(GL_CULL_FACE);
		
		/* Enable Depth Test */
		this.gl.glEnable(GL_DEPTH_TEST);
		this.gl.glDepthFunc(GL_LEQUAL);
		
		initDefaultProgram();
		initDitherLightingProgram();
		
		if((error = this.gl.glGetError()) != 0){
			System.out.println("ERROR: Error after program setup: "+ error);
		} else {
			//System.out.println("DEBUG: Programs successfully created");
		}
		
		initUniformBuffers();
		
		initStampVAO();
		
		initLightTexture();
		
		initInternalFramebuffers();
		
		this.gl.setSwapInterval(1);
		
		if(this.config.getRenderingCallback() != null) {
			this.config.getRenderingCallback().init(this);
		}
	}
	
	private void initDefaultProgram(){
		Shader spriteVS = loadShader("res/shader/sprite_vs.glsl" , GL4.GL_VERTEX_SHADER);
		Shader spriteFS = loadShader("res/shader/sprite_hue_fs.glsl", GL4.GL_FRAGMENT_SHADER);
		this.defaultProgram = new ShaderProgram(spriteVS, spriteFS);
		this.defaultProgram.linkProgram(this.gl);
	}
	
	private void initDitherLightingProgram(){
		Shader spriteVS = loadShader("res/shader/sprite_vs.glsl" , GL4.GL_VERTEX_SHADER);
		Shader spriteFS = loadShader("res/shader/sprite_dither_fs.glsl", GL4.GL_FRAGMENT_SHADER);
		this.ditherLightingProgram = new ShaderProgram(spriteVS, spriteFS);
		this.ditherLightingProgram.linkProgram(this.gl);
	}
	
	private void initUniformBuffers(){
		int defaultEnvIndex = this.defaultProgram.getUniformBlockIndex("Environment", this.gl);
		int defaultSpriteIndex = this.defaultProgram.getUniformBlockIndex("Sprite", this.gl);		
		int defaultColorIndex = this.defaultProgram.getUniformBlockIndex("Color", this.gl);
			
		int ditherEnvIndex = this.ditherLightingProgram.getUniformBlockIndex("Environment", this.gl);
		int ditherSpriteIndex = this.ditherLightingProgram.getUniformBlockIndex("Sprite", this.gl);		
		int ditherColorIndex = this.ditherLightingProgram.getUniformBlockIndex("Color", this.gl);
		
		//System.out.println("DEBUG: Env: "+ envIndex + " Sprite: "+spriteIndex + " Color: "+colorIndex);
		
		int error;
		if((error = this.gl.glGetError()) != 0){
			System.out.println("ERROR: Error getting attribute indices: "+ error);
		} else {
			//System.out.println("DEBUG: Attribute indices successfully located!");
		}
		
		if(this.config == null){
			System.err.println("ERROR: Configuration is null!");
		}
		
		this.envBuffer = this.generateBuffer();
		this.sprBuffer = this.generateBuffer();
		this.colorBuffer = this.generateBuffer();
		
		FloatBuffer env = FloatBuffer.allocate(2);
		env.put(new float[]{this.config.getCanvasWidth(), this.config.getCanvasHeight()});
		env.flip();
		
		this.gl.glUniformBlockBinding(this.defaultProgram.getID(), defaultEnvIndex, 0);
		this.gl.glUniformBlockBinding(this.ditherLightingProgram.getID(), ditherEnvIndex, 0);
		
		if((error = this.gl.glGetError()) != 0){
			System.out.println("ERROR: Error while trying to connect environment buffer to program: "+ error);
		} else {
			//System.out.println("DEBUG: Environment buffer successfully connected");
		}
		
		this.gl.glBindBufferBase(GL4.GL_UNIFORM_BUFFER, 0, envBuffer);
		this.gl.glBufferData(GL4.GL_UNIFORM_BUFFER, ENV_UBO_SIZE * 4 , env, GL4.GL_DYNAMIC_DRAW);
		this.gl.glBindBuffer(GL4.GL_UNIFORM_BUFFER, 0);
		
		FloatBuffer sprite = FloatBuffer.allocate(10);
		sprite.put(new float[]{0,0,0,0,0,0,0,0,0,0});
		sprite.flip();

		this.gl.glUniformBlockBinding(this.defaultProgram.getID(), defaultSpriteIndex, 1);
		this.gl.glUniformBlockBinding(this.ditherLightingProgram.getID(), ditherSpriteIndex, 1);
		
		this.gl.glBindBufferBase(GL4.GL_UNIFORM_BUFFER, 1, sprBuffer);
		this.gl.glBufferData(GL4.GL_UNIFORM_BUFFER, SPRITE_UBO_SIZE * 4, sprite, GL4.GL_DYNAMIC_DRAW);
		this.gl.glBindBuffer(GL4.GL_UNIFORM_BUFFER, 0);
		
		if((error = this.gl.glGetError()) != 0){
			System.out.println("ERROR: Error while trying to connect sprite buffer to program: "+ error);
		} else {
			//System.out.println("DEBUG: Sprite buffer successfully connected");
		}
		
		FloatBuffer color = FloatBuffer.allocate(4);
		color.put(new float[]{0,0,0,0});
		color.flip();

		this.gl.glUniformBlockBinding(this.defaultProgram.getID(), defaultColorIndex, 2);
		this.gl.glUniformBlockBinding(this.ditherLightingProgram.getID(), ditherColorIndex, 2);
		
		this.gl.glBindBufferBase(GL4.GL_UNIFORM_BUFFER, 2, colorBuffer);
		this.gl.glBufferData(GL4.GL_UNIFORM_BUFFER, COLOR_UBO_SIZE * 4, color, GL4.GL_DYNAMIC_DRAW);
		this.gl.glBindBuffer(GL4.GL_UNIFORM_BUFFER, 0);
		
		if((error = this.gl.glGetError()) != 0){
			System.out.println("ERROR: Error while trying to connect color buffer to program: "+ error);
		} else {
			//System.out.println("DEBUG: Color buffer successfully connected");
		}
	}
	
	private void initStampVAO(){
		int error;
		
		float[] vertices = {
				0f, 0f,
				0f, 1f,
				1f, 1f,
				1f, 0f,
				};

		
		FloatBuffer vBuffer = FloatBuffer.allocate(vertices.length);
		vBuffer.put(vertices);
		vBuffer.flip();
		
		byte[] indices = {
				0, 1, 2,
				2, 3, 0
		};
		
		ByteBuffer iBuffer = ByteBuffer.allocate(indices.length).order(ByteOrder.nativeOrder());
		iBuffer.put(indices);
		iBuffer.flip();
		
		int vaoID = generateVertexArray();
		int vboID = generateBuffer();
		int vboiID = generateBuffer();
		
		if((error = this.gl.glGetError()) != 0){
			System.out.println("ERROR: Error after setting up VAO or VBA: "+ error);
		} else {
			//System.out.println("DEBUG: VBA and VBOs created");
		}
		
		/* Debugging */
//		int pid = this.defaultProgram.getID();
//		
//		int vertexLoc = this.gl.glGetAttribLocation(pid, "vertex");
//		
//		//System.out.println("DEBUG: Location of attribute \"vertex\": " + vertexLoc);
//		
//		String[] names = { "position", "size", "position_in_texture", "size_in_texture" };
//
//		IntBuffer b = IntBuffer.allocate(4);
//		this.gl.glGetUniformIndices(pid, 4, names, b); 
//
//		IntBuffer o = IntBuffer.allocate(4);
//		this.gl.glGetActiveUniformsiv(pid, 4, b, GL_UNIFORM_OFFSET, o);
//		
//		for(int i = 0; i < 4 ; i++){
//			System.out.println("DEBUG: Uniform Block Position Offset for: "+names[i]+": "+b.get(i)+": "+o.get(i));
//		}
		/* End Debugging */
		
		/* Buffer Data into the Buffer */
		this.gl.glBindBuffer(GL4.GL_ARRAY_BUFFER, vboID);
		this.gl.glBufferData(GL4.GL_ARRAY_BUFFER, vertices.length * 4, vBuffer, GL4.GL_STATIC_DRAW);
		/* Buffer Data into the Buffer */
		this.gl.glBindBuffer(GL4.GL_ELEMENT_ARRAY_BUFFER, vboiID);
		this.gl.glBufferData(GL4.GL_ELEMENT_ARRAY_BUFFER, indices.length, iBuffer, GL4.GL_STATIC_DRAW);
		
		/* Bind VAO */
		this.gl.glBindVertexArray(vaoID);
		/* Enable linking the just bound VBO to the input of a shader */
		this.gl.glEnableVertexAttribArray(0);
		this.gl.glVertexAttribPointer(0, 2, GL4.GL_FLOAT, false, 0, 0L);
		
		this.gl.glBindBuffer(GL4.GL_ARRAY_BUFFER, vboID);
		this.gl.glBindBuffer(GL4.GL_ELEMENT_ARRAY_BUFFER, vboiID);
		
		/* UnBind for safety reasons ... */
		this.gl.glBindVertexArray(0);
		
		this.spriteVAO = vaoID;
	}
	
	private void initInternalFramebuffers(){
		int error;
		this.sceneFB = createFramebuffer(this.config.getCanvasWidth(), this.config.getCanvasHeight());
//		this.gl.glActiveTexture(GL_TEXTURE1);
//		this.gl.glBindTexture(GL_TEXTURE_2D, this.sceneFB.getTexture().getID());
//		this.gl.glActiveTexture(GL_TEXTURE0);
		this.sceneSprite = new Sprite(this.sceneFB.getTexture());
		
		this.lightFB = createFramebuffer(this.config.getCanvasWidth(), this.config.getCanvasHeight());
		this.gl.glActiveTexture(GL_TEXTURE1);
		this.gl.glBindTexture(GL_TEXTURE_2D, this.lightFB.getTexture().getID());
		this.gl.glActiveTexture(GL_TEXTURE0);
		
		if((error = this.gl.glGetError()) != 0){
			System.out.println("ERROR: Error after init process: "+ error);
		} else {
			//System.out.println("DEBUG: Internal Framebuffers successfully created!");
		}
		this.gl.glUseProgram(this.ditherLightingProgram.getID());
		int scene = this.gl.glGetUniformLocation(this.ditherLightingProgram.getID(), "scene");
		int light = this.gl.glGetUniformLocation(this.ditherLightingProgram.getID(), "light");
		this.gl.glUniform1i(scene, 0);
		this.gl.glUniform1i(light, 1 );
	}
	
	/*
	@Deprecated
	private void initDitherMask(){
		// this'll stay here if I might need it later again
		// create the texture ID for this texture
		this.gl.glGenTextures(1, this.ID_BUFFER);
		int textureID = this.ID_BUFFER.get(0);
		
		System.out.println("INFO: New Texture ID: " + textureID);
		
		// bind this texture
		this.gl.glActiveTexture(GL_TEXTURE1);
		this.gl.glBindTexture(GL_TEXTURE_2D, textureID);

		ByteBuffer textureBuffer = ByteBuffer.allocateDirect(4 * 4 * 4); //4 Pixel * 4 Pixel * 4 Byte
		byte[] texture = {
			0,0,0,(byte)255,0,0,0,(byte)128,0,0,0,(byte)255, 0,0,0,(byte)24,
			0,0,0,(byte)255,0,0,0,(byte)255,0,0,0,(byte)230, 0,0,0,(byte)255,
			0,0,0,(byte)255,0,0,0,(byte)72,0,0,0,(byte)255, 0,0,0,(byte)200,
			0,0,0,(byte)255,0,0,0,(byte)255,0,0,0,(byte)255, 0,0,0,(byte)255,
		};

		textureBuffer.put(texture);
		textureBuffer.flip();
		
		gl.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		gl.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		gl.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		gl.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

		// produce a texture from the byte buffer
		gl.glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA,
				4, 4, 0, GL_RGBA,
				GL_UNSIGNED_BYTE, textureBuffer);
		
		int error;
		if((error = gl.glGetError()) != 0){
			System.out.println("ERROR: Error creating texture: "+ error);
		} else {
			//System.out.println("DEBUG: VBA and VBOs created");
		}
		
		//this.dither = new Texture(textureID, 4, 4, 4, 4);
		
		this.gl.glActiveTexture(GL_TEXTURE0);
	}
	*/
	
	private void initLightTexture(){
		this.light = this.db.loadMipmappedTexture("res/assets/glow2.png", this.gl);
	}
	
	@Override
	public Texture createTexture(String url) {
		return this.db.loadTexture(url, this.gl);
	}
	
	@Override
	public Texture createMipmappedTexture(String url) {
		return this.db.loadMipmappedTexture(url, this.gl);
	}
	
	@Override
	public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
		//GL4 gl = drawable.getGL().getGL4();
	}
	
	private int generateBuffer(){
		this.gl.glGenBuffers(1, this.ID_BUFFER);
		return this.ID_BUFFER.get(0);
	}
	
	private int generateVertexArray(){
		this.gl.glGenVertexArrays(1, this.ID_BUFFER);
		return this.ID_BUFFER.get(0);
	}
	
	private Shader loadShader(String file, int shaderType){
		String string = "";
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(SpritesUtility.loadResource(file)));
			String line;
			while((line = reader.readLine()) != null){
//DEBUG			System.out.println(line);
				string += line + "\n";
			}
		} catch (IOException e){
			e.printStackTrace();
			System.exit(-1);
		}
		
		Shader shader = new Shader(string, shaderType);
//DEBUG	System.out.println("New Shader ID: "+ shaderID);
		if(!shader.compileShader(this.gl)){
			Log.printLog();
		}
		
		return shader;
	}
	
}