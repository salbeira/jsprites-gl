package sprites.base;

import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.io.IOException;

import javax.media.opengl.awt.GLCanvas;
import javax.swing.JFrame;

import sprites.Configuration;
import sprites.util.SpritesUtility;

public class SpritesWindow extends JFrame implements Renderer{
	
	private static final long serialVersionUID = 42L;
	
	private GLCanvas canvas;
	private Configuration config;
	
	private GraphicsEnvironment ge;
	private GraphicsDevice gd;
	private GraphicsConfiguration gc;
	
	public SpritesWindow(Configuration configuration, GLCanvas canvas) {
		/* System */
		this.ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		this.gd = ge.getDefaultScreenDevice();
		this.gc = gd.getDefaultConfiguration();
		
		/* */
		this.config = configuration;
		this.canvas = canvas;
		
		if(this.canvas == null){
			throw new IllegalStateException("GLCanvas is a nullpointer");
		}

		this.canvas.setFocusTraversalKeysEnabled(false);
		this.canvas.setFocusable(true);
		this.canvas.requestFocus();
//		this.canvas.setAutoSwapBufferMode(true);
		this.canvas.setSize(this.config.getWindowWidth(), this.config.getWindowHeight());
		
		this.add(this.canvas);
		
		try {
			this.setIconImage(SpritesUtility.loadImage("res/assets/sprites.png"));
		} catch (IOException e) {
			System.err.println("Fatal error: Failed to load basic sprites icon image");
			System.exit(-1);
		}
		
		this.setResizable(this.config.isWindowResizable());
		
		if(!this.config.isFullscreen()){
			this.setUndecorated(this.config.isUndecorated());
			this.setLocation(this.gc.getBounds().width / 2 - this.config.getWindowWidth() / 2, gc.getBounds().height / 2 - this.config.getWindowHeight() / 2);
		}
	}
	
	@Override
	public void render(){
		if(this.canvas != null){
			this.canvas.display();
		}
	}
}