package sprites.base.ogl;

public class BufferObject {
	
	private int id;
	
	public BufferObject(int id){
		this.id = id;
	}
	
	public int getID(){
		return id;
	}
}