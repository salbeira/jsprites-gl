package sprites.base.ogl;

import sprites.Texture;

public class Framebuffer {

	private int framebufferID;
	private Texture texture;
	
	public Framebuffer(int framebufferID, Texture texture){
		this.framebufferID = framebufferID;
		this.texture = texture;
	}
	
	public int getID(){
		return this.framebufferID;
	}
	
	public Texture getTexture(){
		return this.texture;
	}
}
