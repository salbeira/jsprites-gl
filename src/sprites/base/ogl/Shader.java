package sprites.base.ogl;

import java.nio.ByteBuffer;
import java.nio.IntBuffer;

import javax.media.opengl.GL4;

import sprites.util.Log;

public class Shader {

	private String source;
	private int type;
	private int id;
	
	public Shader(String source, int type){
		this.source = source;
		this.type = type;
		this.id = 0;
	}
	
	public boolean compileShader(GL4 gl){
		this.id = gl.glCreateShader(this.type);
		gl.glShaderSource(this.id, 1, new String[] {this.source}, null);
		gl.glCompileShader(this.id);
		
		IntBuffer successBuffer = IntBuffer.allocate(1);
		gl.glGetShaderiv(this.id,  GL4.GL_COMPILE_STATUS, successBuffer);
		int success = successBuffer.get();
		
		if(success != GL4.GL_TRUE){
			
			IntBuffer lengthBuffer = IntBuffer.allocate(1);
			gl.glGetShaderiv(this.id, GL4.GL_INFO_LOG_LENGTH, lengthBuffer);
			int length = lengthBuffer.get();
			
			ByteBuffer characterBuffer = ByteBuffer.allocate(length);
			gl.glGetShaderInfoLog(this.id, length, lengthBuffer, characterBuffer);
			
			String infoLog = "";
			for(int i = 0 ; i < length; i++){
				infoLog += (char) characterBuffer.get(i);
			}
			System.err.println("Error while compiling shader "+this.id);
			Log.log(infoLog);
			this.id = 0;
			return false;
		}
		return true;
	}
	
	public boolean isCompiled(){
		return this.id != 0;
	}
	
	public int getID(){
		return this.id;
	}
}