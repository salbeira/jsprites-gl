package sprites.base.ogl;

import java.nio.ByteBuffer;
import java.nio.IntBuffer;

import javax.media.opengl.GL4;

import sprites.util.Log;

public class ShaderProgram {

	private int id;
	private Shader vertexShader;
	private Shader fragmentShader;

	/**
	 * 
	 * @param vertexShader
	 * @param fragmentShader
	 */
	public ShaderProgram(Shader vertexShader, Shader fragmentShader) {
		this.vertexShader = vertexShader;
		this.fragmentShader = fragmentShader;
		this.id = 0;
	}

	/**
	 * 
	 * @return
	 */
	public int getID() {
		return this.id;
	}

	/**
	 * 
	 * @param gl
	 */
	public void linkProgram(GL4 gl) {
		this.id = gl.glCreateProgram();
		gl.glAttachShader(this.id, this.vertexShader.getID());
		gl.glAttachShader(this.id, this.fragmentShader.getID());

		gl.glLinkProgram(this.id);

		IntBuffer successBuffer = IntBuffer.allocate(1);
		gl.glGetProgramiv(this.id, GL4.GL_LINK_STATUS, successBuffer);
		int success = successBuffer.get();

		if (success != GL4.GL_TRUE) {

			IntBuffer lengthBuffer = IntBuffer.allocate(1);
			gl.glGetProgramiv(this.id, GL4.GL_INFO_LOG_LENGTH, lengthBuffer);
			int length = lengthBuffer.get();

			ByteBuffer characterBuffer = ByteBuffer.allocate(length);
			gl.glGetProgramInfoLog(this.id, 512, lengthBuffer, characterBuffer);

			String infoLog = "";
			for (int i = 0; i < length; i++) {
				infoLog += (char) characterBuffer.get(i);
			}
			Log.log(infoLog);
			this.id = 0;
		}
	}

	/**
	 * INTERNAL METHOD! Use Engine.getUniformBuffer(ShaderProgram, String) to
	 * get the value!
	 * 
	 * @param name
	 * @param gl
	 */
	public int getUniformBlockIndex(String name, GL4 gl) {
		if (!this.isLinked()) {
			throw new IllegalStateException(
					"This program is not linked! Either you did not call linkProgram(GL4 gl), or the linking process failed!");
		}
		int index = gl.glGetUniformBlockIndex(id, name);
		if (index == GL4.GL_INVALID_INDEX) {
			System.err.println("OpenGL Error: Invalid Index: " + index);
		}
		return index;
	}

	public boolean isLinked() {
		return this.id != 0;
	}
}