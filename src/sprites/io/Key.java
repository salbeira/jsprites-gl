package sprites.io;

import java.util.LinkedList;
import java.util.List;

public class Key {
	
	private String name;
	private Integer keyCode;
	
	public List<KeyObserver> observerList;
	
	public Key(String name, Integer keyCode){
		this.name = name;
		this.keyCode = keyCode;
		this.observerList = new LinkedList<KeyObserver>();
	}
	
	public void press(){
		this.observerList.stream().forEach(element -> element.keyPressed(this));
	}
	
	public void release(){
		this.observerList.stream().forEach(element -> element.keyReleased(this));
	}
	
	public void subscribe(KeyObserver observer){
		if(this.observerList.contains(observer)) return;
		this.observerList.add(observer);
	}
	
	public void unsubscribe(KeyObserver observer){
		if(this.observerList.contains(observer)) this.observerList.remove(observer);
	}
	
	public String getName(){
		return this.name;
	}
	
	public Integer getKeyCode(){
		return this.keyCode;
	}
}