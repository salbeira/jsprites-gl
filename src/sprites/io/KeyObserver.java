package sprites.io;

public interface KeyObserver {
	
	/**
	 * Called whenever a key is pressed.
	 * @param key The key that was pressed.
	 */
	public void keyPressed(Key key);
	
	/**
	 * Called whenever a key is released.
	 * @param key The key that was released.
	 */
	public void keyReleased(Key key);
	
}
