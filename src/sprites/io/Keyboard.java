package sprites.io;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Keyboard {
	
	private Map<String, Key> keyIdentifcationMap;
	private Map<Integer, Key> keyCodeMap;
	
	private List<Character> characterBuffer;
	
	public Keyboard(){
		this.keyIdentifcationMap = new HashMap<String, Key>();
		this.keyCodeMap = new HashMap<Integer, Key>();
		this.characterBuffer = new LinkedList<Character>();
	}
	
	/**
	 * Creates a new Key within the Keyboard handling structure that listens to
	 * keystrokes done that correspond to Java Swing KeyEvent's keycodes.
	 * @param name An identifier that acts as an abstract name for the action associated with the interaction with the key, like "jump" or "shoot" or the like.
	 * @param keyCode The virtual key code of the java VK_ type, like VK_A for triggering on presses of the A button.
	 */
	public void createNewKey(String name, Integer keyCode){
		Key key = new Key(name, keyCode);
		this.keyIdentifcationMap.put(name, key);
		this.keyCodeMap.put(keyCode, key);
	}
	
	public Key getKey(Integer keyCode){
		return this.keyCodeMap.get(keyCode);
	}
	
	public Key getKey(String name){
		return this.keyIdentifcationMap.get(name);
	}
	
	public List<Character> getCharacterBuffer(){
		return this.characterBuffer;
	}
}