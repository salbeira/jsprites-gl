package sprites.io;

import java.util.LinkedList;
import java.util.List;

public class Mouse {
	
	public enum Button{
		LEFT, MIDDLE, RIGHT, EXTRA1, EXTRA2
	}
	
	private List<MouseObserver> observerList;
	private List<MouseObserver> containing;
	
	public Mouse(){
		this.observerList = new LinkedList<MouseObserver>();
		this.containing = new LinkedList<MouseObserver>();
	}
	
	public void move(int x, int y){
		for(MouseObserver observer : this.observerList){
			if(observer.contains(x, y)){
				if(containing.contains(observer)){
					observer.moved(x, y);
				} else {
					containing.add(observer);
					observer.entered(x, y);
				}
			} else if(this.containing.contains(observer)){
				observer.left(x, y);
				containing.remove(observer);
			}
		}
	}
	
	public void drag(int x, int y){
		for(MouseObserver observer : this.observerList){
			if(observer.contains(x, y)){
				if(containing.contains(observer)){
					observer.dragged(x, y);
				} else {
					containing.add(observer);
					observer.entered(x, y);
				}
			} else if(this.containing.contains(observer)){
				containing.remove(observer);
				observer.left(x, y);
			}
		}
	}
	
	public void press(Button button){
		for(MouseObserver observer : this.containing){
			observer.buttonPressed(button);
		}
	}
	
	public void release(Button button){
		for(MouseObserver observer : this.containing){
			observer.buttonReleased(button);
		}
	}
	
	public void subscribe(MouseObserver observer){
		if(this.observerList.contains(observer)) return;
		this.observerList.add(observer);
	}
	
	public void unsubscribe(MouseObserver observer){
		if(this.observerList.contains(observer)) this.observerList.remove(observer);
	}
}