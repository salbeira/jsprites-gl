package sprites.io;

public interface MouseObserver {

	/**
	 * Implement this method to let the mouse check weather or not this object contains the cursor.
	 * @param x The x position of the mouse within the window
	 * @param y The y position of the mouse within the window
	 * @return Weather or not this object contains the mouse.
	 */
	public boolean contains(int x, int y);
	
	/**
	 * Called when this object contains the mouse and a mouse button is pressed.
	 * @param button The button that was pressed.
	 */
	public void buttonPressed(Mouse.Button button);
	
	/**
	 * Called when this object contains the mouse and a mouse button is released.
	 * @param button The button that was released.
	 */
	public void buttonReleased(Mouse.Button button);
	
	/**
	 * Called when the cursor entered this object.
	 * @param x The x position the mouse entered this object.
	 * @param y The y position the mouse entered this object.
	 */
	public void entered(int x, int y);
	
	/**
	 * Called when the cursor left this object.
	 * @param x The x position the mouse left this object.
	 * @param y The y position the mouse left this object.
	 */
	public void left(int x, int y);
	
	/**
	 * Called when the cursor moved within this object.
	 * @param x The new x position of the mouse.
	 * @param y The new y position of the mouse.
	 */
	public void moved(int x, int y);
	
	/**
	 * Called when the cursor was dragged within this object.
	 * @param x The new x position of the mouse.
	 * @param y The new y position of the mouse.
	 */
	public void dragged(int x, int y);
}
