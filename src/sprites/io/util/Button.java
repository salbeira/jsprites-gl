package sprites.io.util;

import sprites.Engine;
import sprites.Sprite;
import sprites.io.MouseObserver;
import sprites.util.BitmapFont;

public abstract class Button implements MouseObserver {

	protected enum State {
		NORMAL, FOCUSED, ACTIVE
	}

	protected Sprite normal;
	protected Sprite focused;
	protected Sprite active;

	protected BitmapFont font;

	protected String label;

	protected State state;

	public Button(Sprite normal, Sprite focused, Sprite active) {
		this(normal, focused, active, "", null);
	}

	public Button(Sprite normal, Sprite focused, Sprite active, String label,
			BitmapFont font) {
		this.normal = normal;
		this.focused = focused;
		this.active = active;
		this.state = State.NORMAL;
		this.label = label == null ? "" : label;
		this.font = font;
	}

	public abstract void click();

	public abstract int getX();

	public abstract int getY();

	public void render(Engine renderer) {
		switch (this.state) {
		case NORMAL:
			renderer.renderSprite(this.normal, this.getX(), this.getY());
			if (!this.label.equals("") && this.font != null) {
				renderer.renderStringCentered(this.label, this.getX()
						+ (this.normal.getWidth() / 2), this.getY()
						+ (this.normal.getHeight() / 2), this.font);
			}
			break;
		case FOCUSED:
			renderer.renderSprite(this.focused, this.getX(), this.getY());
			if (!this.label.equals("") && this.font != null) {
				renderer.renderStringCentered(this.label, this.getX()
						+ (this.focused.getWidth() / 2), this.getY()
						+ (this.focused.getHeight() / 2), this.font);
			}
			break;
		case ACTIVE:
			renderer.renderSprite(this.active, this.getX(), this.getY());
			if (!this.label.equals("") && this.font != null) {
				renderer.renderStringCentered(this.label, this.getX()
						+ (this.active.getWidth() / 2), this.getY()
						+ (this.active.getHeight() / 2), this.font);
			}
			break;
		}
	}

	@Override
	public void buttonPressed(sprites.io.Mouse.Button button) {
		if (button.equals(sprites.io.Mouse.Button.LEFT)) {
			this.state = State.ACTIVE;
		}
	}

	@Override
	public void buttonReleased(sprites.io.Mouse.Button button) {
		if (button.equals(sprites.io.Mouse.Button.LEFT)) {
			if (this.state == State.ACTIVE) {
				this.click();
			}
		}
		this.state = State.FOCUSED;
	}

	@Override
	public void dragged(int x, int y) {
	}

	@Override
	public void entered(int x, int y) {
		this.state = State.FOCUSED;
	}

	@Override
	public void left(int x, int y) {
		this.state = State.NORMAL;
	}

	@Override
	public void moved(int x, int y) {
	}
}