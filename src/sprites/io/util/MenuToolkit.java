package sprites.io.util;

import sprites.Sprite;
import sprites.util.BitmapFont;

public class MenuToolkit {

	public static class Borders{
		public final Sprite topLeft;
		public final Sprite topRight;
		public final Sprite bottomLeft;
		public final Sprite bottomRight;
		
		public Borders(Sprite topLeft, Sprite topRight, Sprite bottomLeft, Sprite bottomRight){
			this.topLeft = topLeft;
			this.topRight = topRight;
			this.bottomLeft = bottomLeft;
			this.bottomRight = bottomRight;
		}
	}
	
	public static class Sides{
		public final Sprite top;
		public final Sprite bottom;
		public final Sprite left;
		public final Sprite right;
		
		public Sides(Sprite top, Sprite bottom, Sprite left, Sprite right){
			this.top = top;
			this.bottom = bottom;
			this.left = left;
			this.right = right;
		}
	}
	
	private Borders borders;
	private Sides sides;
	private BitmapFont font;
	
	public MenuToolkit(Borders borders, Sides sides, BitmapFont font){
		this.borders = borders;
		this.sides = sides;
		this.font = font;
	}
	
	
}
