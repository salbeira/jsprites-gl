package sprites.io.util;

import sprites.Sprite;
import sprites.util.BitmapFont;

/**
 * 
 * @author Hauer
 *
 */
public abstract class RadioButton extends Button {
	
	protected RadioButtonGroup group;
	
	protected State activeState;
	
	RadioButton(Sprite normal, Sprite focused, Sprite active, RadioButtonGroup group) {
		this(normal, focused, active, "", null, group);
	}
	
	RadioButton(Sprite normal, Sprite focused, Sprite active, String label, BitmapFont font, RadioButtonGroup group) {
		super(normal, focused, active, label, font);
		if(group == null){
			throw new IllegalArgumentException("The RadioButtonGroup was null --- A RadioButton has to belong to a group!");
		}
		this.group = group;
		this.activeState = State.NORMAL;
	}
	
	@Override
	public void click() {
		this.group.buttonClicked(this);
		this.activeState = State.ACTIVE;
	}
	
	public void deactivate(){
		this.activeState = State.NORMAL;
	}
	
	@Override
	public void left(int x, int y) {
		this.state = this.activeState;
	}
	
}