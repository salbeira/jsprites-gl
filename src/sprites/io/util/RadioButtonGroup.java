package sprites.io.util;

import java.util.LinkedList;
import java.util.List;

/**
 * Utility Class for managing groups of RadioButtons. Also serves as a factory for those.
 * 
 * @author Hauer
 *
 */
public class RadioButtonGroup {

	private RadioButton activeButton;
	private List<RadioButton> buttons;
	
	public RadioButtonGroup(){
		this.activeButton = null;
		this.buttons = new LinkedList<RadioButton>();
	}
	
	/**
	 * Creates a new RadioButton in this group.
	 * @return
	 */
	public void add(RadioButton button){
		this.buttons.add(button);
	}
	
	public RadioButton getActiveButton(){
		return this.activeButton;
	}
	
	public void buttonClicked(RadioButton button){
		for(RadioButton other : buttons){
			if(!other.equals(button)){
				other.deactivate();
			}
		}
		this.activeButton = button;
	}
	
}