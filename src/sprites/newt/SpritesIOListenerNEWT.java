package sprites.newt;

import sprites.Configuration;
import sprites.io.Key;
import sprites.io.Keyboard;
import sprites.io.Mouse;

import com.jogamp.newt.event.KeyEvent;
import com.jogamp.newt.event.KeyListener;
import com.jogamp.newt.event.MouseEvent;
import com.jogamp.newt.event.MouseListener;

public class SpritesIOListenerNEWT implements MouseListener, KeyListener {
	
	private Configuration config;
	
	public SpritesIOListenerNEWT(Configuration configuration) {
		this.config = configuration;
	}
	
	@Override
	public void keyPressed(KeyEvent e) {
		//Error Handling ...
		if(e == null){
			return;
		}
		checkKeyboard();
		Key key = this.config.getKeyboard().getKey((int) e.getKeyCode());
		if(key != null){
			key.press();
		}
	}
	
	@Override
	public void keyReleased(KeyEvent e) {
		//Error Handling ...
		if(e == null){
			return;
		}
		checkKeyboard();
		Key key = this.config.getKeyboard().getKey((int) e.getKeyCode());
		if(key != null){
			key.release();
		}
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {}
	
	@Override
	public void mouseDragged(MouseEvent e) {
		//Error Handling ...
		if(e == null){
			return;
		}
		checkMouse();
		int x = e.getX();
		int y = e.getY();
		float qx = (float) this.config.getCanvasWidth() / (float) this.config.getWindowWidth();
		float qy = (float) this.config.getCanvasHeight() / (float) this.config.getWindowHeight();
		x *= qx;
		y *= qy;
		this.config.getMouse().move(x,y);
	}
	
	@Override
	public void mouseEntered(MouseEvent e) {
		//Error Handling ...
		if(e == null){
			return;
		}
		
	}
	
	@Override
	public void mouseExited(MouseEvent e) {
		//Error Handling ...
		if(e == null){
			return;
		}
	}
	
	@Override
	public void mouseMoved(MouseEvent e) {
		//Error Handling ...
		if(e == null){
			return;
		}
//		System.out.println("Mouse: "+e.getX()+ ", " + e.getY());
		checkMouse();
		int x = e.getX();
		int y = e.getY();
		float qx = (float) this.config.getCanvasWidth() / (float) this.config.getWindowWidth();
		float qy = (float) this.config.getCanvasHeight() / (float) this.config.getWindowHeight();
		x *= qx;
		y *= qy;
		this.config.getMouse().move(x,y);
	}
	
	@Override
	public void mousePressed(MouseEvent e) {
		//Error Handling ...
		if(e == null){
			return;
		}
		checkMouse();
		Mouse mouse = this.config.getMouse();
		int button = e.getButton();
		switch(button){
			case MouseEvent.BUTTON1 : mouse.press(Mouse.Button.LEFT); break;
			case MouseEvent.BUTTON2 : mouse.press(Mouse.Button.MIDDLE); break;
			case MouseEvent.BUTTON3 : mouse.press(Mouse.Button.RIGHT); break;
		}
	}
	
	@Override
	public void mouseReleased(MouseEvent e) {
		//Error Handling ...
		if(e == null){
			return;
		}
		checkMouse();
		Mouse mouse = this.config.getMouse();
		int button = e.getButton();
		switch(button){
			case MouseEvent.BUTTON1 : mouse.release(Mouse.Button.LEFT); break;
			case MouseEvent.BUTTON2 : mouse.release(Mouse.Button.MIDDLE); break;
			case MouseEvent.BUTTON3 : mouse.release(Mouse.Button.RIGHT); break;
		}
	}
	
	@Override
	public void mouseWheelMoved(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	private void checkConfig(){
		if(this.config == null){
			throw new IllegalStateException("Fatal error: Configuration is a nullpointer!");
		}
	}
	
	private void checkKeyboard(){
		checkConfig();
		Keyboard keyboard = this.config.getKeyboard();
		if(keyboard == null){
			throw new IllegalStateException("Fatal error: Keyboard within Configuration is a nullpointer!");
		}
	}
	
	private void checkMouse(){
		checkConfig();
		Mouse mouse  = this.config.getMouse();
		if(mouse == null){
			throw new IllegalStateException("Fatal error: Mouse within Configuration is a nullpointer!");
		}
	}
	
}
