package sprites.util;

import sprites.Texture;
import sprites.util.BitmapFont;

public class BavariaRegularBitmapFont extends BitmapFont{

	private static int[][] widths = {
			{6,6,6,6,6,3,6,6,2,4,5,3,8,6,6,6,6,5,6,4,6,6,8,6,6,5},
			{6,6,6,6,5,5,6,6,4,6,6,5,8,6,6,6,6,6,6,6,6,6,8,6,6,6},
			{2,2,4,6,6,8,6,2,3,3,6,4,2,4,2,8,4,4,5,5,6,5,5,5,5,5,2,3,4,4,4,5},
			{8,4,8,4,6,5,4,4,2,4,5}
	};
	
	public BavariaRegularBitmapFont(Texture texture){
		super(texture, 10, widths);
	}
}