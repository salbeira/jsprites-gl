package sprites.util;

import java.util.LinkedList;
import java.util.List;

public class Log {

	private static String logged ="";
	private static List<LogObserver> obs = new LinkedList<LogObserver>();
	
	/**
	 * Logs the String and notifies every observer about it.
	 * @param log
	 */
	public static void log(String log){
		logged += (log);
		for(LogObserver o : obs){
			o.notify(log);
		}
	}
	
	/**
	 * Logs the character, but does not notify anything.
	 * @param c
	 */
	public static void log(char c){
		logged += c;
	}
	
	/**
	 * Prints out the log.
	 */
	public static void printLog(){
		System.out.println(logged);
	}
	
	public static void addObserver(LogObserver observer){
		obs.add(observer);
	}
}
