package sprites.util;

public interface LogObserver {

	public void notify(String log);
	
}
