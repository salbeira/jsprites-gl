package sprites.util;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import javax.imageio.ImageIO;

public class SpritesUtility {

	/**
	 * Load a given resource as a buffered image
	 * 
	 * @param ref
	 *            The location of the resource to load
	 * @return The loaded buffered image
	 * @throws IOException
	 *             Indicates a failure to find a resource
	 */
	public static BufferedImage loadImage(String resource) throws IOException {
		URL url = SpritesUtility.class.getClassLoader().getResource(resource);
		if (url == null) {
			throw new IOException("Cannot find: " + resource);
		}
		Image img = ImageIO.read(url);
		BufferedImage bufferedImage = new BufferedImage(img.getWidth(null),
				img.getHeight(null), BufferedImage.TYPE_INT_ARGB);
		Graphics g = bufferedImage.getGraphics();
		g.drawImage(img, 0, 0, null);
		g.dispose();

		return bufferedImage;
	}
	
	public static InputStream loadResource(String file) throws IOException {
		URL url = SpritesUtility.class.getClassLoader().getResource(file);
		if(url == null){
			throw new IOException("Cannot find: " + file);
		}
		return url.openStream();
	}
}